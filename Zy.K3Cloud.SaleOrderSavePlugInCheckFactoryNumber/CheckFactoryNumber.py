import clr
clr.AddReference('Kingdee.BOS')
clr.AddReference('Kingdee.BOS.Core')
clr.AddReference('Kingdee.BOS.DataEntity')

from Kingdee.BOS.Core.DynamicForm.PlugIn.ControlModel import *
from Kingdee.BOS.Orm.DataEntity import *
from Kingdee.BOS.Core import *

class zyKingCloudSaleOrderSaveCheck:
	def CheckCount(entityObj):
		fn = "" if entityObj['F_HY_Text1'] is None else entityObj['F_HY_Text1'].ToString()
		count = int(entityObj['Qty'])
		fntype = "" if entityObj['F_ora_fn_type'] is None else entityObj['F_ora_fn_type'].ToString()
		fn_count = 0

		if( fntype == ""): return True;

		if( fn != ""):
			fns = fn.split('-')
			if (len(fns) == 1):
				fn_count = 1
			elif len(fns) == 2:
				fn_count = int(fns[1]) - int(fns[0]) + 1
			else:
				fn_count = -999
			return fn_count == count
		elif( fntype != ""):
			return False;
		else:
			return True;

	doSave = False;

	def BeforeSave(e):
		#base.BeforeSave(e);
		super(zyKingCloudSaleOrderSaveCheck, self).BeforeSave(e);

		global doSave
		if(doSave):
			doSave = False; return;

		e.Cancel = True
		entity = this.View.BusinessInfo.GetEntity("FSaleOrderEntry")
		entityObjs = this.View.Model.GetEntityDataObject(entity)
		if (not (entityObjs is None)):
			index = 0

			for entityObj in entityObjs:
				if(CheckCount(entityObj) == False):
					this.View.ShowErrMessage("行号：{0} 选择了编码类别{1}，并且销售数量{2}与出厂编码{3}范围不相符，请修改数量或重新生成出厂编码！".format(index,entityObj['F_ora_fn_type'],entityObj['Qty'],entityObj['F_HY_Text1']));
					return;
	
				index = index + 1

		doSave = True;
		this.View.InvokeFormOperation("Save");